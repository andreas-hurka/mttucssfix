<!--
SPDX-FileCopyrightText: 2023 Andreas Hurka <>

SPDX-License-Identifier: GPL-3.0-or-later
-->

# mttucssfix



## What even is this?

mttucssfix is little more than a collection of CSS rules aimed at making moodle.taltech.ee more usable on desktop. This is done by reducing unnecessary padding, utilizing more of the available space and making areas that look like buttons clickable in their entirety.

These rules are also packaged into a Firefox extension.

## Installation

In order to install this extension, head to the [Releases](https://gitlab.com/andreas-hurka/mttucssfix/-/releases) page and select the latest (or any other release) and download the file listed under "Packages". Firefox should ask you whether you want to install the add-on automatically. Note that as of now, the releases do not support automatic updates.

### Why not addons.mozilla.org?

Add-ons published on addons.mozilla.org need to be applicable to a wide audience. Since this add-on is only relevant to people involved with Taltech, it cannot be published there.

## Getting started

Installing the extension as described above is all you need to do. Once that is done Moodle should look different, but you might have to reload any open tabs.

If you want to use these rules without completely installing the add-on you can clone this repository to your machine, start Firefox and head to about:debugging. There check out the "This Firefox" section, an option load a temporary addon should be available, just point this to the `manifest.json` file. Note that this needs to be repeated for every start of Firefox.

Of course, you are also free to experiment with other ways of introducing these rules to your browsing experience.

In order to build the extension into a .zip archive (on linux) the following command can be used: `zip -r -FS ../mttucssfix.zip * --exclude '*.git*' --exclude '*.kra*'` Note that this differs from the command in the [Firefox Extension Workshop](https://extensionworkshop.com/documentation/publish/package-your-extension/), as it also excludes krita files and their licensing information.

## Troubleshooting

Should you notice any strange behaviour while browsing moodle with this extension active, first try disabling mttucssfix and reloading the page. Should this fix the behaviour feel free to open an issue or dig into the code yourself.

## REUSE

This project is licensed under [GPL-3.0-or-later](https://spdx.org/licenses/GPL-3.0-or-later.html). Its source code is hosted on [https://gitlab.com/andreas-hurka/mttucssfix](https://gitlab.com/andreas-hurka/mttucssfix).

Should you decide to contribute, please make sure that your contributions uphold [REUSE](https://reuse.software/tutorial/) compliance.

## Project Status

This project has had its initial release and is somewhat active.
